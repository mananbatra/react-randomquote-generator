import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {

  const randomQuotes = [
    "Success is not final, failure is not fatal: It is the courage to continue that counts. - Winston Churchill",
    "The only limit to our realization of tomorrow will be our doubts of today. - Franklin D. Roosevelt",
    "Life is what happens when you're busy making other plans. - John Lennon",
    "The future belongs to those who believe in the beauty of their dreams. - Eleanor Roosevelt",
    "The only way to do great work is to love what you do. - Steve Jobs",
    "In three words I can sum up everything I've learned about life: it goes on. - Robert Frost",
    "Believe you can and you're halfway there. - Theodore Roosevelt",
    "The best way to predict the future is to create it. - Peter Drucker",
    "The only person you are destined to become is the person you decide to be. - Ralph Waldo Emerson",
    "Life is 10% what happens to us and 90% how we react to it. - Charles R. Swindoll",
    "Don't watch the clock; do what it does. Keep going. - Sam Levenson",
    "It's not whether you get knocked down, it's whether you get up. - Vince Lombardi",
    "The future depends on what we do in the present. - Mahatma Gandhi",
    "The only thing standing between you and your goal is the story you keep telling yourself as to why you can't achieve it. - Jordan Belfort",
    "You miss 100% of the shots you don't take. - Wayne Gretzky",
    "Don't cry because it's over, smile because it happened. - Dr. Seuss",
    "You can't use up creativity. The more you use, the more you have. - Maya Angelou",
    "Happiness is not something ready-made. It comes from your own actions. - Dalai Lama",
    "The secret of getting ahead is getting started. - Mark Twain",
    "Life isn't about finding yourself. It's about creating yourself. - George Bernard Shaw",
    "The only limit to our realization of tomorrow will be our doubts of today. - Franklin D. Roosevelt",
    "It does not matter how slowly you go as long as you do not stop. - Confucius",
    "What you get by achieving your goals is not as important as what you become by achieving your goals. - Zig Ziglar",
    "You have power over your mind - not outside events. Realize this, and you will find strength. - Marcus Aurelius",
    "In the end, it's not the years in your life that count. It's the life in your years. - Abraham Lincoln",
    "The only way to do great work is to love what you do. - Steve Jobs",
    "Don't wait. The time will never be just right. - Napoleon Hill",
    "You must be the change you wish to see in the world. - Mahatma Gandhi",
    "Whatever you can do, or dream you can, begin it. Boldness has genius, power, and magic in it. - Johann Wolfgang von Goethe",
    "Believe you can and you're halfway there. - Theodore Roosevelt",
    "The best way to predict the future is to create it. - Peter Drucker",
    "The future belongs to those who believe in the beauty of their dreams. - Eleanor Roosevelt",
    "The only person you are destined to become is the person you decide to be. - Ralph Waldo Emerson",
    "I haven't failed. I've just found 10,000 ways that won't work. - Thomas A. Edison",
  ];
  const [quote, setQuote] = useState(randomQuotes[0])

  function randomQuoteGenerator()
  {
    const random=randomQuotes[Math.floor(Math.random()*randomQuotes.length)]
    
    setQuote(random)

  }

  return (
    <>
      <div>{quote}</div>
      <button onClick={()=>randomQuoteGenerator()}>Click to see Random Quotes</button>
    </>
  )
}

export default App
